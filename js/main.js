/*
    Attempt to remove the annoying elements promoting the reddit app. If more
    are added to the DOM after this JS runs, they will be hidden and disabled
    using CSS.
*/

const elementsToRemove = document.querySelectorAll('.xpromoMinimal, .XPromoPill, .XPromoPopup, .TopButton');

while(elementsToRemove.length > 0) {
    elementsToRemove[0].parentNode.removeChild(elementsToRemove[0]);
}