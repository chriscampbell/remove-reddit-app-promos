# Remove reddit app promos

A small Firefox extension which removes the banners and promos trying to get you to install the Reddit app, these banners and promos normally appear when browsing Reddit on a mobile device.

[Install the extension](https://addons.mozilla.org/en-US/firefox/addon/remove-reddit-app-promos/)